package com.bankapplication.talan.service;

import java.math.BigDecimal;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import com.bankapplication.talan.exception.BankException;
import com.bankapplication.talan.model.Account;
import com.bankapplication.talan.model.TransactionHistory;

public class TransactionHistoryCommonTest {

    protected final TransactionHistoryService transactionHistoryService;

    protected final AccountService accountService;

    protected TransactionHistory transactionHistory;

    protected static Account accountA;
    protected static Account accountB;
    protected static Account accountC;
    protected static Account accountD;

    public TransactionHistoryCommonTest(TransactionHistoryService transactionHistoryService,
	    AccountService accountService) {
	this.transactionHistoryService = transactionHistoryService;
	this.accountService = accountService;
    }

    @BeforeEach
    void setup() {
	accountA = accountService.saveAccount(new Account("500"));
	accountB = accountService.saveAccount(new Account("500"));
	accountC = accountService.saveAccount(new Account("500"));
	accountD = accountService.saveAccount(new Account("500"));
    }

    static class TransactionHistoryFromAccountArgument implements ArgumentsProvider {

	@Override
	public Stream<Arguments> provideArguments(ExtensionContext context) {
	    return Stream.of(Arguments.of(accountA, 4), Arguments.of(accountB, 3), Arguments.of(accountC, 0));
	}
    }

    static class TransactionHistoryToAccountArgument implements ArgumentsProvider {

	@Override
	public Stream<Arguments> provideArguments(ExtensionContext context) {
	    return Stream.of(Arguments.of(accountA, 2), Arguments.of(accountB, 3), Arguments.of(accountC, 2));
	}
    }

    protected TransactionHistory makeTransfer(Account payer, Account payee, BigDecimal amount) throws BankException {
	transactionHistory = transactionHistoryService.create(payer.getAccountNum(), payee.getAccountNum(), amount);
	return transactionHistory;
    }
}
