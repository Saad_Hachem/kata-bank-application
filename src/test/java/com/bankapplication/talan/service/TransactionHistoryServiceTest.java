package com.bankapplication.talan.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bankapplication.talan.exception.BankException;
import com.bankapplication.talan.exception.ExceptionCode;
import com.bankapplication.talan.model.Account;
import com.bankapplication.talan.model.TransactionHistory;
import com.bankapplication.talan.model.TransactionStatut;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
@ActiveProfiles("test")
public class TransactionHistoryServiceTest extends TransactionHistoryCommonTest {

    @Autowired
    public TransactionHistoryServiceTest(TransactionHistoryService transactionHistoryService,
	    AccountService accountService) {
	super(transactionHistoryService, accountService);
    }

    @Nested
    class CreateTransactionHistoryTest {

	@Test
	void should_return_successful_transaction_history() throws BankException {
	    makeTransfer(accountA, accountB, new BigDecimal("500"));

	    assertAll(() -> assertNotNull(transactionHistory, "Transaction Not Created"),
		    () -> assertEquals(transactionHistory.getTransactionStatut(), TransactionStatut.SUCCEEDED));
	}

	@Test
	void should_return_failed_transaction_history() throws BankException {
	    makeTransfer(accountA, accountC, new BigDecimal("900"));

	    assertAll(() -> assertNotNull(transactionHistory, "Transaction Not Created"),
		    () -> assertEquals(transactionHistory.getTransactionStatut(), TransactionStatut.FAILED));
	}

	@Test
	void should_return_failed_transaction_history_if_payer_make_transfer_to_himslef() throws BankException {
	    makeTransfer(accountA, accountA, new BigDecimal("100"));

	    assertAll(() -> assertNotNull(transactionHistory, "Transaction Not Created"),
		    () -> assertEquals(transactionHistory.getTransactionStatut(), TransactionStatut.FAILED));
	}

	@Test
	void should_throw_failed_transaction_history_for_null_amount() throws BankException {
	    makeTransfer(accountA, accountB, null);

	    assertAll(() -> assertNotNull(transactionHistory, "Transaction Not Created"),
		    () -> assertEquals(transactionHistory.getTransactionStatut(), TransactionStatut.FAILED));
	}

	@Test
	void should_throw_exception_for_account_not_exist() throws BankException {
	    Account accountNotSaved = Account.builder().accountNum(2765765).build();

	    BankException exception = assertThrows(BankException.class,
		    () -> makeTransfer(accountA, accountNotSaved, new BigDecimal("100")));

	    assertAll(() -> assertNull(transactionHistory),
		    () -> assertEquals(exception.getCode(), ExceptionCode.ACCOUNT_NOT_FOUND));
	}

    }

    @Nested
    class QueryTransactionHistoryTest {

	@BeforeEach
	void setup() throws BankException {
	    // accountA : 4 payer , 2 payee
	    // accountB : 3 payer , 3 payee
	    // accountC : 0 payer , 2 payee
	    // accountD : 0 payer , 0 payee

	    makeTransfer(accountA, accountB, new BigDecimal("10"));
	    makeTransfer(accountA, accountB, new BigDecimal("20"));
	    makeTransfer(accountA, accountB, new BigDecimal("30"));
	    makeTransfer(accountA, accountC, new BigDecimal("40"));

	    makeTransfer(accountB, accountA, new BigDecimal("40"));
	    makeTransfer(accountB, accountA, new BigDecimal("90"));
	    makeTransfer(accountB, accountC, new BigDecimal("130"));
	}

	@Test
	void should_return_zero_transaction_history() {
	    int resultSize = getSizeHistoryByFunction(transactionHistoryService::findAllByPayerId, accountD);

	    assertEquals(0, resultSize);
	}

	@ParameterizedTest
	@ArgumentsSource(TransactionHistoryFromAccountArgument.class)
	void should_return_correct_number_of_transaction_history_form_account(Account account, int excpectedSize) {
	    int resultSize = getSizeHistoryByFunction(transactionHistoryService::findAllByPayerId, account);

	    assertEquals(excpectedSize, resultSize);
	}

	@ParameterizedTest
	@ArgumentsSource(TransactionHistoryToAccountArgument.class)
	void should_return_correct_number_of_transaction_history_to_account(Account account, int excpectedSize) {
	    int resultSize = getSizeHistoryByFunction(transactionHistoryService::findAllByPayeeId, account);

	    assertEquals(excpectedSize, resultSize);
	}

	@Test
	void should_return_correct_number_of_transaction_history_from_acount_to_account() {
	    int nbThAccountBtoC = transactionHistoryService
		    .findAllByPayerIdAndPayeeId(accountB.getAccountNum(), accountC.getAccountNum()).size();

	    assertEquals(nbThAccountBtoC, 1);
	}

	private int getSizeHistoryByFunction(Function<Integer, List<TransactionHistory>> function, Account account) {
	    return function.apply(account.getAccountNum()).size();
	}

    }

}
