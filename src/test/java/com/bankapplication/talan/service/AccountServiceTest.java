package com.bankapplication.talan.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bankapplication.talan.exception.BankException;
import com.bankapplication.talan.exception.ExceptionCode;
import com.bankapplication.talan.model.Account;

import lombok.RequiredArgsConstructor;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
@ActiveProfiles("test")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
class AccountServiceTest {

    private final AccountService accountService;

    @Nested
    class WthdrawAndDepositTest {
	private Account account;

	@BeforeEach
	void setup() {
	    this.account = accountService.saveAccount(new Account("500"));
	}

	@Test
	void should_return_correct_amount_after_withdraw_money() throws BankException {
	    Account result = accountService.withdraw(account.getAccountNum(), new BigDecimal("100"));
	    assertThat(result.getBalance(), Matchers.comparesEqualTo(new BigDecimal("400")));
	}

	@Test
	void should_return_correct_amount_after_withdraw_money_with_precision() throws BankException {
	    // Round to 100.45
	    Account result = accountService.withdraw(account.getAccountNum(), new BigDecimal("100.446"));
	    assertThat(result.getBalance(), Matchers.comparesEqualTo(new BigDecimal("399.55")));
	}

	@Test
	void should_throw_exception_for_withdraw_amount_bigger_than_balance() {
	    BankException exception = assertThrows(BankException.class,
		    () -> accountService.withdraw(account.getAccountNum(), new BigDecimal("600")));

	    assertEquals(exception.getCode(), ExceptionCode.BALANCE_NOT_ENOUGHT);
	}

	@Test
	void should_throw_exception_for_withdraw_zero_amount() {
	    BankException exception = assertThrows(BankException.class,
		    () -> accountService.withdraw(account.getAccountNum(), BigDecimal.ZERO));

	    assertEquals(exception.getCode(), ExceptionCode.INCORRECT_AMOUNT);
	}

	@Test
	void should_throw_exception_for_withdraw_null_amount() {
	    BankException exception = assertThrows(BankException.class,
		    () -> accountService.withdraw(account.getAccountNum(), null));

	    assertEquals(exception.getCode(), ExceptionCode.INCORRECT_AMOUNT);
	}

	@Test
	void should_throw_exception_for_withdraw_negative_amount() {
	    BankException exception = assertThrows(BankException.class,
		    () -> accountService.withdraw(account.getAccountNum(), new BigDecimal("-100")));

	    assertEquals(exception.getCode(), ExceptionCode.INCORRECT_AMOUNT);
	}

	@Test
	void should_return_correct_amount_after_deposit_money() throws BankException {
	    Account result = accountService.deposit(account.getAccountNum(), new BigDecimal("100"));
	    assertThat(result.getBalance(), Matchers.comparesEqualTo(new BigDecimal("600")));
	}

	@Test
	void should_throw_exception_for_deposit_zero_amount() {
	    BankException exception = assertThrows(BankException.class,
		    () -> accountService.deposit(account.getAccountNum(), BigDecimal.ZERO));

	    assertEquals(exception.getCode(), ExceptionCode.INCORRECT_AMOUNT);
	}

	@Test
	void should_throw_exception_for_deposit_null_amount() {
	    BankException exception = assertThrows(BankException.class,
		    () -> accountService.deposit(account.getAccountNum(), null));

	    assertEquals(exception.getCode(), ExceptionCode.INCORRECT_AMOUNT);
	}
    }

    @Nested
    class TransferTest {

	private Account payer;
	private Account payee;

	@BeforeEach
	void setup() {
	    payer = accountService.saveAccount(new Account("500"));
	    payee = accountService.saveAccount(new Account("500"));
	}

	@Test
	void should_return_correct_balance_for_each_account() throws BankException {
	    accountService.transfer(payer.getAccountNum(), payee.getAccountNum(), new BigDecimal("200"));

	    payer = accountService.getAccount(payer.getAccountNum());
	    payee = accountService.getAccount(payee.getAccountNum());

	    assertAll(() -> assertThat(payer.getBalance(), Matchers.comparesEqualTo(new BigDecimal("300"))),
		    () -> assertThat(payee.getBalance(), Matchers.comparesEqualTo(new BigDecimal("700"))));
	}

	@Test
	void should_throw_exception_for_transfer_with_zero_amount() throws BankException {
	    BankException exception = assertThrows(BankException.class,
		    () -> accountService.transfer(payer.getAccountNum(), payee.getAccountNum(), new BigDecimal("0")));

	    assertEquals(exception.getCode(), ExceptionCode.INCORRECT_AMOUNT);
	}

	@Test
	void should_throw_exception_for_transfer_with_negative_amount() throws BankException {
	    BankException exception = assertThrows(BankException.class, () -> accountService
		    .transfer(payer.getAccountNum(), payee.getAccountNum(), new BigDecimal("-100")));

	    assertEquals(exception.getCode(), ExceptionCode.INCORRECT_AMOUNT);
	}

	@Test
	void should_throw_exception_if_payer_make_transfer_to_himslef() {
	    BankException exception = assertThrows(BankException.class,
		    () -> accountService.transfer(payer.getAccountNum(), payer.getAccountNum(), new BigDecimal("100")));

	    assertEquals(exception.getCode(), ExceptionCode.TRANSFER_ACCOUNT_UNAUTHORISED);
	}

	@Test
	void should_throw_exception_if_payee_not_exist() {
	    BankException exception = assertThrows(BankException.class,
		    () -> accountService.transfer(payer.getAccountNum(), -1, new BigDecimal("100")));

	    assertEquals(exception.getCode(), ExceptionCode.ACCOUNT_NOT_FOUND);
	}
    }

}
