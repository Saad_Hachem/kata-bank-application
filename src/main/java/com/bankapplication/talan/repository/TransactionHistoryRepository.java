package com.bankapplication.talan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bankapplication.talan.model.TransactionHistory;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, Integer> {

    List<TransactionHistory> findAllByPayerAccountNum(Integer fromAccountNum);
    
    List<TransactionHistory> findAllByPayeeAccountNum(Integer toAccountNum);
    
    List<TransactionHistory> findAllByPayerAccountNumAndPayeeAccountNum(Integer fromAccountNum,Integer toAccountNum);
}
