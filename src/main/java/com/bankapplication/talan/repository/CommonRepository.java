package com.bankapplication.talan.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CommonRepository<T, E> extends JpaRepository<T, E> {

    default T findOne(E id) {
	return (T) findById(id).orElse(null);
    }
}
