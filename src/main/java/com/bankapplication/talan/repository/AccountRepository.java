package com.bankapplication.talan.repository;

import com.bankapplication.talan.model.Account;

public interface AccountRepository extends CommonRepository<Account, Integer> {

}