package com.bankapplication.talan.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bankapplication.talan.exception.BankException;
import com.bankapplication.talan.model.Account;
import com.bankapplication.talan.model.TransactionHistory;
import com.bankapplication.talan.repository.TransactionHistoryRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TransactionHistoryService {

    private final AccountService accountService;

    private final TransactionHistoryRepository transactionHistoryRepository;

    public TransactionHistory create(Integer payerId, Integer payeeId, BigDecimal amount) throws BankException {
	Account payer = accountService.getAccount(payerId);
	Account payee = accountService.getAccount(payeeId);
	
	TransactionHistory transactionHistory = TransactionHistory.builder().payer(payer).payee(payee).amount(amount)
		.build();
	
	// catch to create TransactionHistory even the transfer failed
	try {
	    accountService.transfer(payerId, payeeId, amount);
	} catch (Exception e) {
	    transactionHistory.failed(e.getMessage());
	}

	return saveTransactionHistory(transactionHistory);
    }

    public TransactionHistory saveTransactionHistory(TransactionHistory transactionHistory) {
	return transactionHistoryRepository.save(transactionHistory);
    }

    public List<TransactionHistory> findAllByPayeeId(Integer accountId) {
	return transactionHistoryRepository.findAllByPayeeAccountNum(accountId);
    }

    public List<TransactionHistory> findAllByPayerId(Integer accountId) {
	return transactionHistoryRepository.findAllByPayerAccountNum(accountId);
    }

    public List<TransactionHistory> findAllByPayerIdAndPayeeId(Integer payerAccountId, Integer payeeAccountId) {
	return transactionHistoryRepository.findAllByPayerAccountNumAndPayeeAccountNum(payerAccountId, payeeAccountId);
    }

}
