package com.bankapplication.talan.service;

import java.math.BigDecimal;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Service;

import com.bankapplication.talan.exception.BankException;
import com.bankapplication.talan.exception.ExceptionCode;
import com.bankapplication.talan.model.Account;
import com.bankapplication.talan.repository.AccountRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor

public class AccountService {

    private final AccountRepository accountRepository;

    public Account withdraw(Integer string, BigDecimal amount) throws BankException {
	Account account = getAccount(string);
	account.withdraw(amount);
	return saveAccount(account);
    }

    public Account deposit(Integer accountNum, BigDecimal amount) throws BankException {
	Account account = getAccount(accountNum);
	account.deposit(amount);
	return saveAccount(account);
    }

    @Transactional
    public void transfer(Integer payerId, Integer payeeId, BigDecimal amount) throws BankException {
	Account payer = getAccount(payerId);
	Account payee = getAccount(payeeId);

	payer.transfer(amount, payee);

	saveAccount(payer);
	saveAccount(payee);
    }

    public Account getAccount(Integer accountNum) throws BankException {
	Account account = accountRepository.findOne(accountNum);
	if (account == null)
	    throw new BankException(ExceptionCode.ACCOUNT_NOT_FOUND);
	return account;
    }

    public Account saveAccount(@NotNull Account account) {
	return accountRepository.save(account);
    }

}
