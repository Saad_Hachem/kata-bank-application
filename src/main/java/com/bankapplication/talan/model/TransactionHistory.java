package com.bankapplication.talan.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Data
@NoArgsConstructor

public class TransactionHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer TransactionHistoryNum;

    @NonNull
    @ManyToOne
    private Account payer;

    @ManyToOne
    private Account payee;

    @NonNull
    @Column(scale = 2)
    private BigDecimal amount;

    @Enumerated(EnumType.ORDINAL)
    private TransactionStatut transactionStatut = TransactionStatut.SUCCEEDED;

    @CreationTimestamp
    private LocalDateTime dateTime;

    private String failureMessage;

    @Builder
    public TransactionHistory(Account payer, Account payee, BigDecimal amount) {
	this.payer = payer;
	this.payee = payee;
	this.amount = amount;
    }

    public void failed(String failureMessage) {
	this.failureMessage = failureMessage;
	this.transactionStatut = TransactionStatut.FAILED;
    }

}
