package com.bankapplication.talan.model;

public enum TransactionStatut {
    SUCCEEDED, FAILED
}
