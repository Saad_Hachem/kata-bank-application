package com.bankapplication.talan.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.bankapplication.talan.exception.BankException;
import com.bankapplication.talan.exception.ExceptionCode;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer accountNum;

    @Column(scale = 2)
    private BigDecimal balance;

    @OneToMany
    private Set<TransactionHistory> transactionHistories;

    public Account(String balance) {
	this.balance = new BigDecimal(balance).setScale(2, RoundingMode.HALF_EVEN);
    }
    
    @Builder
    public Account(Integer accountNum) {
	this.accountNum = accountNum;
    }

    public void withdraw(BigDecimal amount) throws BankException {
	checkAmountNotNegative(amount);
	checkEnoughBalance(amount);

	setBalance(balance.subtract(amount));
    }

    public void deposit(BigDecimal amount) throws BankException {
	checkAmountNotNegative(amount);

	setBalance(balance.add(amount));
    }

    public void transfer(BigDecimal amount, Account payee) throws BankException {
	if (equals(payee))
	    throw new BankException(ExceptionCode.TRANSFER_ACCOUNT_UNAUTHORISED);
	withdraw(amount);
	payee.deposit(amount);
    }

    private void checkAmountNotNegative(BigDecimal amount) throws BankException {
	// check if amount <= 0
	if (amount == null || amount.compareTo(BigDecimal.ZERO) < 1)
	    throw new BankException(ExceptionCode.INCORRECT_AMOUNT);
    }

    private void checkEnoughBalance(BigDecimal amount) throws BankException {
	if (amount == null || balance.compareTo(amount) == -1)
	    throw new BankException(ExceptionCode.BALANCE_NOT_ENOUGHT);
    }

    private void setBalance(BigDecimal balance) {
	if (balance != null)
	    this.balance = balance.setScale(2, RoundingMode.HALF_EVEN);
    }

}