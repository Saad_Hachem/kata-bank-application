package com.bankapplication.talan.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ExceptionCode {
    TRANSFER_ACCOUNT_UNAUTHORISED(1, "The payee must be different from the payer"), INCORRECT_AMOUNT(2, "Incorrect Amount"),
    BALANCE_NOT_ENOUGHT(3, "Balance is not enough"), ACCOUNT_NOT_FOUND(4, "Account not found");

    private final int id;

    private final String message;
}
