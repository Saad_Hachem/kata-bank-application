package com.bankapplication.talan.exception;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
public class BankException extends Exception {

    private static final long serialVersionUID = 2607136422865194255L;

    private ExceptionCode code;

    public BankException(ExceptionCode code) {
	super(code.getMessage());
	this.code = code;
	log.error(code.getMessage());
    }

    public BankException(Throwable cause, ExceptionCode code) {
	super(code.getMessage(), cause);
	this.code = code;
	log.error(code.getMessage());
    }

}
