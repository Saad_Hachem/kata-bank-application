# Kata Bank Account
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/f51c49aa3faa4306b96e3ff233d86501)](https://www.codacy.com/gl/Saad_Hachem/kata-bank-application/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=Saad_Hachem/kata-bank-application&amp;utm_campaign=Badge_Grade)
# User Stories

Bank Account Api that manages a bank account
- Write some code to create a bank application and to withdraw/deposit a valid
amount of money in/from the account

- Write some code to transfer a specified amount of money from one bank
account (the payer) to another (the payee)

- Write some code to keep a record of the transfer for both bank accounts in a
transaction history

- Write some code to query a bank account's transaction history for any bank
transfers to or from a specific account

# Built With
- Spring Boot 2.4
- Java 11
- Junit 5
- Gradle 6
- Lombok 1.8
- H2 Database

# Presentation
The kata was performed as part of a **TDD methodology**, as we have noticed in the user stories it is requested to set up an application to manage the operations of a bank account (**withdraw**, **deposit**,**transfer**) and to manage the **transactions** linked to a bank transfer.

We will need two principal test classes to check the user stories :
1. **AccountServiceTest** : Test class to check the different scenarios that can happen during banking operations linked to an account
2. **TransactionHistoryServiceTest** : Test class to check the different scenarios that can happen during the creation or consultation of banking transaction histories linked to an account

To ensure the proper functioning of the application we will need :

- Entities
    1. **Account** : object to store the information related to the account (balance ...) and to manage each operation needed
    2. **TransactionHistory** : object to store the information related to a transaction history (payer,payee,amount ...)
- Repositories
    1. **AccountRepository** : Account CRUD operations
    2. **TransactionHistoryRepository** : TransactionHistory CRUD operations
- Services
    1. **AccountService**
    2. **TransactionHistoryService**
- Exception
    1. **BankException** : Handle all the exceptions linked to scenarios not managed or not allowed in the application

# Notes
1. **Money** is represented with BigDecimal to have more control over precision and rounding
2. **RoundingMode.HALF_EVEN** as explained in the java documentation it's sometimes known as "Banker's rounding"
3. **Hamcrest** api is used to to compare correctly the BigDecimal type as it contains utilities more suitable for this